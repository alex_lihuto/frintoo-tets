<?php

class tournament
{
    private $name, $date;

    private $players = [];

    public function __construct($name, $date = null)
    {
        $this->name = $name;
        if ($date != null) {
            $this->date = $date;
            return;
        }
        $date = strtotime("now");
        $this->date = date("d.m.Y", $date);
    }

    public function addPlayer($player)
    {
        $name = $player->getName();
        $city = $player->getCity();
        $this->players[] = $name . $city;

        return $this;
    }

    public function createPairs()
    {
        $a = $this->players;
        $currentDate = $this->date;
        if (count($a) % 2 > 0) {
            $this->players = '10';
        }
        for ($x = 0; $x < count($a) - 1; $x++) {
            $lk = array_key_last($a);
            $day_team = [];
            for ($i = 0; $i < count($a) / 2; $i++) {
                if($a[$i] == $a[$lk]) continue;
                elseif ($a[$i] != '10' && $a[$lk] != '10') {
                    $day_team[] = $a[$i] . ' - ' . $a[$lk];
                }
                $lk--;
            }
            $last = array_pop($a);
            array_splice($a, 1, 0, $last);
            $date = str_replace(".", "-", $this->date);
            $timestamp = strtotime($date);
            $this->date = date("d.m.Y", strtotime("+1 day", $timestamp));
            array_splice($day_team, 0, 0, $this->name . ', ' . $this->date);
            $team_days[$x] = $day_team;
        }

        $this->printResult($team_days);
    }

    public function printResult($team_days)
    {
        echo '<br>';
        foreach ($team_days as $team_day) {
            foreach ($team_day as $row) {
                echo $row . '<br>';
            }
        }
        echo '<br>';
    }
}